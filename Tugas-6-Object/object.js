var dataArray = [
    ["Abduh", "Muhamad", "male", 1992],
    ["Ahmad", "Taufik", "male", 1985]
]
function arrayToObject(dataArray) {
    var now = new  Date()
    var thisYear = now.getFullYear()
    
    for(var i = 0; i < dataArray.length; i++) {
        if(!dataArray[i][3] || dataArray[i][3] > thisYear) {
            age = "Invalid Birth Year"
        }
        else {
            age = thisYear - dataArray[i][3]
        }

        var dataObject = {
            firstName : dataArray[i][0],
            lastName : dataArray[i][1],
            gender : dataArray[i][2],
            age : age
        }
        console.log(`${dataObject.firstName} ${dataObject.lastName} : ${JSON.stringify(dataObject)}`)
    }
}

arrayToObject(dataArray)

//tugas 2

let shoppingTime = (memberId, money) => {
    if(!memberId){
        return `Mohon maaf, toko X hanya berlaku untuk member saja`;
    } else if(money < 50000){
        return `Mohon maaf, uang tidak cukup`;
    } else {
        let obj = {};
        let listPurchased = [];
        let changeMoney = money;
        let sepatu = {item: 'Sepatu stacattu',harga: 1500000};
        let zoro = {item: 'Baju Zoro',harga: 500000};
        let hn = {item: 'Baju H&N', harga: 250000};
        let uniklooh = {item: 'Sweater Uniklooh', harga: 175000};
        let casing = {item: 'Casing Handphone', harga: 50000};

        for(let i = 0; changeMoney >= 50000; i++){
            if(changeMoney >= sepatu.harga){
                listPurchased.push(sepatu.item);
                changeMoney -= sepatu.harga;
            } else if(changeMoney >= zoro.harga){
                listPurchased.push(zoro.item);
                changeMoney -= zoro.harga;
            } else if(changeMoney >= hn.harga){
                listPurchased.push(hn.item);
                changeMoney -= hn.harga;
            } else if(changeMoney >= uniklooh.harga){
                listPurchased.push(uniklooh.item);
                changeMoney -= uniklooh.harga;
            } else if(changeMoney >= casing.harga){
                listPurchased.push(casing.item);
                changeMoney -= casing.harga;
            }
        }
        obj.memberId = memberId;
        obj.money = money;
        obj.listPurchased = listPurchased;
        obj.changeMoney = changeMoney;

        return obj;
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

//tugas 3

function naikAngkot(arrPenumpang) {
    rute = ["A", "B", "C", "D", "E", "F"]
    var biaya = 2000
    var endResult = []

    if(arrPenumpang.length == 0) {
        return arrPenumpang
    }

    for(var i = 0; i < arrPenumpang.length; i++) {
        var penumpangArray = arrPenumpang[i]
        var penumpangObject = {}

        penumpangObject.penumpang = penumpangArray[0]
        penumpangObject.naikDari = penumpangArray[1]
        penumpangObject.tujuan = penumpangArray[2]
        penumpangObject.bayar = biaya * (rute.indexOf(penumpangObject.tujuan) - rute.indexOf(penumpangObject.naikDari))
        
        endResult.push(penumpangObject)
    }
    return endResult
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))