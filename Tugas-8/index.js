var readBooks = require('./callback.js');

var books = [
    {name : 'LOTR', timeSpent:3000},
    {name: 'Fidas', timeSpent:2000},
    {name: 'Kalkulus', timeSpent: 4000}
];

let time = 10000;
let index = 0;

const cb = (remainingTime) => {
    if (remainingTime) {
      if (time == remainingTime) {
        return;
      }
      time = remainingTime;
      index = index + 1;
    }
    if (index != books.length) {
      readBooks(time, books[index], cb);
    }
  };
  
  cb();
  